# -*- coding: utf-8 -*-
# Darkwing4

from os import getcwd, remove, rename
from os.path import exists
from subprocess import call
from linecache import getline
from typing import Dict, List, Union

path = getcwd()
components = path + "\components\\"
temp = path + "\\temp\\"

tank_charges = "5561,5562,5563,5564".split(",") # ДА,ПАЛ,ТК,ШК


########## конфиг под хроники сальвейшин  ##################
NumColumn_Animation = 21              ######################
NumColumn_Icon = 22                   ######################
NumColumn_Type = 24                   #баф\дебаф\бард\тригер
###########################################################

def removeComment(inputStr):
    """
    это костыль, что бы можно было прямо в переменной писать комментарии на русском
    """
    symbols = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '=', ',']
    list_id = ""
    for i in inputStr:
        if i in symbols:
            list_id += i
    return list_id.split(",")

#TODO ЭТО ЕБУЧИЙ БЫДЛОКОД, НАДО ПЕРЕДЕЛАТЬ
def getAnimationOrIcon_ID(skill_ID, numColumn):

    def getNumLine(skill_ID):
        if int(skill_ID) > 55031 or int(skill_ID)<1:
            raise IndexError  # несуществующий айди скила
        if int(skill_ID) <= 1355:
            return 1, 37384
        if int(skill_ID) <= 10273:
            return 37385, 74769
        if int(skill_ID) <= 11534:
            return 74770, 112153
        if int(skill_ID) <= 55031:
            return 112154, 149539

    start, end = getNumLine(skill_ID)
    for numLine in range(start, end):
        line = getline(temp+"inputSkilgrp.txt", numLine).split("	")

        if line[0] == skill_ID:
            if line[numColumn] == "":
                raise KeyError
            return line[numColumn]

def skillgrpEditor(protect_ID, remove_ID, inDebuff_ID, replaceAnimation_ID, replaceIcon_ID,
                   tank_charge_level_adjustment,
                   removeAllAnimation=True):

    # global countLines
    # countLines = sum(1 for line in open(temp+'inputSkilgrp.txt', "r", encoding="UTF-8"))

    if exists("skillgrp.dat"):
        remove("skillgrp.dat")

    #встаем на костыли и трем комменты
    protect_ID = removeComment(protect_ID)
    remove_ID = removeComment(remove_ID)
    inDebuff_ID = removeComment(inDebuff_ID)

    try:
        animationReplace = {}
        for skill_id in removeComment(replaceAnimation_ID):
            skill_id = skill_id.split("=")
            animationReplace[skill_id[0]] = getAnimationOrIcon_ID(skill_id[1], numColumn=NumColumn_Animation)

    except IndexError:
        print("Скилл с ID = {} не обнаружен.".format(skill_id[1]))
    except KeyError:
        print("У указанного скила нет анимации.")
    except TypeError:
        pass

    try:
        iconReplace: Dict[str, Union[str, List[str]]] = {}
        for skill_id in removeComment(replaceIcon_ID):
            skill_id = skill_id.split("=")
            iconReplace[skill_id[0]] = getAnimationOrIcon_ID(skill_id[1], numColumn=NumColumn_Icon)

    except IndexError:
        print("Скилл с ID = {} не обнаружен.".format(skill_id[1]))
    except KeyError:
        print("У указанного скила нет иконки.")
    except TypeError:
        pass

    with open(temp+"skillgrp.txt", "w", encoding="UTF-8") as outputSkilgrpTxt, open(temp+'inputSkilgrp.txt', "r", encoding="UTF-8") as inputSkillgrp:
        for line in inputSkillgrp:

            line = line.split('	')

            skill_ID = line[0]
            skill_lvl = line[1]

            if removeAllAnimation:
                if skill_ID not in protect_ID:
                    line[NumColumn_Animation] = "" # отключаем анимацию

            elif skill_ID in remove_ID:
                line[NumColumn_Animation] = ""  # - - / / - -

            if skill_ID in inDebuff_ID:
                line[NumColumn_Type] = "1" # 1 - будет отображаться как дебаф

            if skill_ID in animationReplace and skill_ID not in remove_ID:
                line[NumColumn_Animation] = animationReplace[skill_ID]

            if skill_ID in iconReplace:
                line[NumColumn_Icon] = iconReplace[skill_ID]

            if skill_ID in tank_charges:
                for i in range(len(tank_charge_level_adjustment)):
                    if skill_ID == tank_charge_level_adjustment[i][0] and skill_lvl ==  tank_charge_level_adjustment[i][1]:
                        line[NumColumn_Animation] = getAnimationOrIcon_ID(tank_charge_level_adjustment[i][2], numColumn=NumColumn_Animation)
                        break

            # сохраняем изменения
            line = '	'.join(line)
            outputSkilgrpTxt.write(line)

    call('{0}l2asm.exe -d {1}skillgrp-new.ddf -q {1}skillgrp.txt {1}dec-skillgrp.dat'.format(components, temp))
    call('{}l2encdec.exe -h 413 {}dec-skillgrp.dat'.format(components, temp))
    rename('enc-dec-skillgrp.dat', 'skillgrp.dat')
