from os import rename
from os.path import exists
from datetime import datetime

pathSystemFolder = "C:/Games/Emerland/system-ru/"


if __name__ == "__main__":
    if exists(pathSystemFolder + "skillgrp.dat"):
        currentTime = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S").replace(":","-")
        rename(pathSystemFolder + "skillgrp.dat", pathSystemFolder + "_backup " + currentTime + " " +  "skillgrp.dat")

    rename("skillgrp.dat", pathSystemFolder + "skillgrp.dat")
